import './App.css';
import { useEffect, useState } from 'react';
import Country from './components/Country/Country';
import Cart from './components/Cart/Cart';

function App() {

  const [countries, setcountries] = useState([]);
  const [country, setCountry] = useState([]);

  useEffect(() => {
    fetch('https://restcountries.eu/rest/v2/all')
      .then(res => res.json())
      .then(data => {
        setcountries(data);
        // console.log(data);
        const countryName = data.map(countries => countries.name)
        // console.log(countryName);
      })
    // .catch(error => console.log(error))
  }, [])

  const handleAddedCountry = (worldName) => {
    const newCountry = [...country, worldName];
    setCountry(newCountry);
  }

  return (
    <div className="App">

      <h2>Total Country : {countries.length}</h2>
      <h3>Board on Country : {country.length}</h3>

      <Cart cart={country}></Cart>

      <ul>
        {
          countries.map(country => <Country key={country.alpha3Code} worldName={country} handleAddedCountry={handleAddedCountry}></Country>)
        }
      </ul>

    </div>
  );
}

export default App;
