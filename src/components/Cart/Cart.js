import React from 'react';

const Cart = (props) => {
    const cartCountry = props.cart;
    // console.log(cartCountry);

    // let totalPopulation = 0;
    // for (let i = 0; i < cartCountry.length; i++) {
    //     const element = cartCountry[i];
    //     totalPopulation = totalPopulation + element.population;

    // }
    const totalPopulation = cartCountry.reduce((sum, country) => sum + country.population, 0)
    return (
        <div>
            <h3>This is Cart : {cartCountry.length}</h3>
            <h5>Country Population : {totalPopulation}</h5>
        </div>
    );
};

export default Cart;