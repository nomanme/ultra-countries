import React from 'react';
import './Country.css';

const Country = (props) => {
    // console.log(props);
    const { name, flag, population, region } = props.worldName;
    const handleAddedCountry = props.handleAddedCountry;

    return (
        <div className="country">
            <img className="flag" src={flag} alt="" />
            <h4>This is a {name}</h4>
            <p>Approximate Population: {population}</p>
            <p><small>Region: {region}</small></p>
            <button className="btn" onClick={() => handleAddedCountry(props.worldName)}>Add Country</button>
        </div>
    );
};

export default Country;